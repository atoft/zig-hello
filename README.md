# zig-hello

Various experiments in learning Zig. Obviously not intended to be production code!

## `string.zig`
I tried implementing a very bad dynamic string type myself.

## `from_text.zig`, `to_text.zig`
A reimplementation of [freeplanet](https://github.com/atoft/freeplanet)'s serialization to and from a custom text format.

## TODO
- Error types, error handling for fromText, how to free allocated memory.
- Use zig tests instead of main.
