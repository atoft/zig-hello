const std = @import("std");
const Allocator = std.mem.Allocator;

/// A naive dynamic string implementation.
/// Probably using ArrayList from the std lib would be better.
pub const NaiveString = struct {
    allocator: *Allocator,
    buffer: []u8,

    /// The actual length in use.
    len: usize,

    const initial_buffer_size = 32;
    const expand_factor = 2;

    pub fn init_empty(allocator: *Allocator) !NaiveString {
        return NaiveString{
            .allocator = allocator,
            .buffer = try allocator.alloc(u8, NaiveString.initial_buffer_size),
            .len = 0,
        };
    }

    pub fn init(literal: []const u8, allocator: *Allocator) !NaiveString {
        var result = NaiveString{
            .allocator = allocator,
            .buffer = try allocator.alloc(u8, std.math.max(NaiveString.initial_buffer_size, literal.len)),
            .len = literal.len,
        };
        std.mem.copy(u8, result.buffer, literal);
        return result;
    }

    pub fn destroy(self: *NaiveString) void {
        self.allocator.destroy(self.buffer.ptr);
    }

    /// Append the contents of a string literal to self.
    pub fn append_literal(self: *NaiveString, literal: []const u8) !void {
        const new_size = self.len + literal.len;

        if (new_size > self.buffer.len) {
            var old_buffer = self.buffer;
            const new_buffer_len = std.math.max(old_buffer.len * NaiveString.expand_factor, self.len + literal.len);

            self.buffer = try self.allocator.alloc(u8, new_buffer_len);
            std.mem.copy(u8, self.buffer, old_buffer);
            std.mem.copy(u8, self.buffer[self.len..], literal);

            self.allocator.destroy(old_buffer.ptr);
            std.log.info("Had to expand the internal buffer. New size {}", .{new_buffer_len});
        } else {
            std.mem.copy(u8, self.buffer[self.len..], literal);
            std.log.info("Appended into the existing buffer.", .{});
        }

        // The new actual string length.
        self.len = self.len + literal.len;
    }

    pub fn str(self: *NaiveString) []const u8 {
        std.debug.assert(self.buffer.len >= self.len);

        return self.buffer[0..self.len];
    }
};
