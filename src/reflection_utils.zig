pub fn isListLike(comptime T: type) bool {
    return comptime @hasField(T, "items") and comptime @hasDecl(T, "addOne") and comptime @hasDecl(T, "pop");
}
