const std = @import("std");
const Allocator = std.mem.Allocator;
const utils = @import("reflection_utils.zig");

// ArrayList(u8) treated like a dynamic string.
const String = std.ArrayList(u8);

pub fn toText(allocator: *Allocator, value: anytype, version: u8) ![]u8 {
    var result = String.init(allocator);

    const Type = @TypeOf(value);

    // Emit the type identifier.
    try std.fmt.format(result.writer(), "{s} v{d}", .{ @typeName(Type), version });

    try toTextInternal(allocator, value, &result, 0);

    return result.toOwnedSlice();
}

fn toTextInternal(allocator: *Allocator, value: anytype, result: *String, depth: u32) !void {
    const Type = @TypeOf(value);
    switch (@typeInfo(Type)) {
        .Struct => |struct_info| {
            // Specialisations
            if (comptime utils.isListLike(Type)) {
                try toTextList(allocator, value.items, result, depth);
            } else {
                try result.appendSlice("\n");
                try appendIndent(result, depth);
                try result.appendSlice("{\n");

                inline for (struct_info.fields) |field| {
                    try appendIndent(result, depth + 1);
                    try std.fmt.format(result.writer(), "{}: ", .{field.name});

                    const field_value: *const field.field_type = &@field(value, field.name);

                    try toTextInternal(allocator, field_value.*, result, depth + 1);

                    try result.appendSlice(";\n");
                }

                try appendIndent(result, depth);
                try result.appendSlice("}");
            }
        },
        .Float => {
            try std.fmt.format(result.writer(), "{e}", .{value});
        },
        .Int => {
            try std.fmt.format(result.writer(), "{d}", .{value});
        },
        .Bool => {
            try result.appendSlice(if (value) "true" else "false");
        },
        .Pointer => |pointer_info| {
            if (pointer_info.size == .Slice) {
                try toTextList(allocator, value, result, depth);
            } else {
                @compileError("Unsupported pointer for writing to text " ++ @typeName(Type));
            }
        },
        else => @compileError("Unsupported type for writing to text  " ++ @typeName(Type)),
    }
}

fn toTextList(allocator: *Allocator, items: anytype, result: *String, depth: u32) !void {
    try result.appendSlice("[");

    for (items) |*element, i| {
        try toTextInternal(allocator, element.*, result, depth + 1);

        if (i < items.len - 1) {
            try result.appendSlice(", ");
        }
    }

    try result.appendSlice("]");
}

fn appendIndent(result: *String, depth: u32) !void {
    var count: u32 = 0;

    while (count < depth) : (count += 1) {
        try result.appendSlice("    ");
    }
}
