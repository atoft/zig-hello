const std = @import("std");
const NaiveString = @import("string.zig").NaiveString;

pub fn fact(x: i32) i32 {
    if (x <= 0) {
        return 1;
    }

    return x * fact(x - 1);
}

pub fn fact_imperative(x: i32) i32 {
    var result: i32 = 1;

    var multiplier = x;
    while (multiplier > 1) : (multiplier -= 1) {
        result *= multiplier;
    }

    return result;
}

const Vec2 = struct {
    x: f32,
    y: f32,

    pub fn dot(self: *const Vec2, other: *const Vec2) f32 {
        return self.x * other.x + self.y * other.y;
    }
};

pub fn List(comptime Type: type) type {
    return struct {
        next: ?*List(Type),
        value: Type,

        pub fn new(value: Type) List(Type) {
            return List(Type){
                .next = null,
                .value = value,
            };
        }

        pub fn cons(self: *List(Type), value: Type) List(Type) {
            return List(Type){
                .next = self,
                .value = value,
            };
        }

        pub fn print(self: *List(Type)) void {
            var current = self;

            while (true) {
                std.log.info("{}", .{current.value});

                if (current.next) |next| {
                    current = next;
                } else {
                    break;
                }
            }
        }
    };
}

pub const Nested = struct {
    foo: u32,
    bar: f32,
};

pub const MyConfig = struct {
    width: u32,
    height: u32,
    fov: f32,
    fullscreen: bool,
    //   flags: []const bool,
    dynamic_stuff: std.ArrayList(f32),
    nested: Nested,
    nested_list: std.ArrayList(Nested),

    const default_flags = [_]bool{ false, false, true };

    pub fn init(allocator: *std.mem.Allocator) MyConfig {
        return MyConfig{
            .width = 1920,
            .height = 1080,
            .fov = 90.0,
            .fullscreen = false,
            //            .flags = default_flags[0..],
            .dynamic_stuff = std.ArrayList(f32).init(allocator),
            .nested = Nested{
                .foo = 1,
                .bar = 2.0,
            },
            .nested_list = std.ArrayList(Nested).init(allocator),
        };
    }

    pub fn deinit(self: *const MyConfig) void {
        self.dynamic_stuff.deinit();
        self.nested_list.deinit();
    }
};

pub fn main() anyerror!void {
    std.log.info("5 factorial is {}", .{fact(5)});
    std.log.info("5 factorial is {} computed imperatively", .{fact_imperative(5)});

    const Local = struct {
        pub fn local_fun(x: i32) void {
            std.log.info("Printing {} from a local function", .{x});
        }
    };

    Local.local_fun(3);
    Local.local_fun(4);

    // TODO Can't declare Vec2 here? Get an error in the method definition.

    const vec_a = Vec2{
        .x = 5.0,
        .y = 1.5,
    };

    const vec_b = Vec2{
        .x = 2.0,
        .y = 3.0,
    };

    // Method just syntactic sugar for function call.
    std.log.info("Dot product method syntax {}", .{vec_a.dot(&vec_b)});
    std.log.info("Dot product function syntax {}", .{Vec2.dot(&vec_a, &vec_b)});

    // Doing some generics.
    var list = List(i32).new(1);

    var list2 = list.cons(2);
    var list3 = list2.cons(3);
    list3.print();

    // Labelled block
    const root: i32 = blk: {
        const a = 1;
        const b = 2;
        const c = -3;
        const solution = (-b + std.math.sqrt(b * b - 4 * a * c)) / (2 * a);
        break :blk solution;
    };

    std.log.info("Result of block {}", .{root});

    // Memory management
    const Allocator = std.heap.GeneralPurposeAllocator(.{}); // <- The type of the allocator factory.

    var allocator = Allocator{}; // <- Instantiation of the allocator factory.
    var galloc = &allocator.allocator; // <- The allocator itself
    defer _ = allocator.deinit(); // <- Cleanup at the end of this function, and discard the return value.

    var heap_vec = try galloc.create(Vec2); // TODO Can initialise here? What is the contents, always zeroed?
    heap_vec.* = Vec2{
        .x = 1.0,
        .y = 1.0,
    };

    std.log.info("Dot product with heap {}", .{heap_vec.dot(&vec_b)});

    defer galloc.destroy(heap_vec);

    var my_string = try NaiveString.init("hello world 👉", galloc);
    defer my_string.destroy();

    try my_string.append_literal(" 👍 again!");

    var string_slice = my_string.str();
    std.log.info("{s}", .{string_slice});

    // We didn't have to copy to get a slice of the internal buffer.
    std.debug.assert(string_slice.ptr == my_string.buffer.ptr);

    try my_string.append_literal("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.");
    std.log.info("{}", .{my_string.str()});

    var config = MyConfig.init(galloc);
    defer config.deinit();

    config.fullscreen = false;
    try config.dynamic_stuff.append(123.5);
    try config.dynamic_stuff.append(-7);
    try config.nested_list.append(Nested{
        .foo = 1,
        .bar = 3.0,
    });
    try config.nested_list.append(Nested{
        .foo = 2,
        .bar = 6.0,
    });

    const toText = @import("to_text.zig").toText;

    const vec_to_text = try toText(galloc, vec_b, 0);
    defer galloc.destroy(vec_to_text.ptr);
    std.log.info("{}", .{vec_to_text});

    const config_to_text = try toText(galloc, config, 0);
    defer galloc.destroy(config_to_text.ptr);

    std.log.info("{}", .{config_to_text});

    const fromText = @import("from_text.zig").fromText;

    const read_vec = fromText(Vec2, 0, vec_to_text, galloc) orelse return;

    const read_config = fromText(MyConfig, 0, config_to_text, galloc) orelse return;
    defer read_config.deinit();

    std.log.info("{}", .{read_vec});
    std.log.info("{}", .{config});
    std.log.info("{}", .{read_config});

    const to_text_again = try toText(galloc, read_config, 0);
    defer galloc.destroy(to_text_again.ptr);

    std.log.info("{}", .{to_text_again});
}
