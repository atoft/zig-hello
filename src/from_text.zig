const std = @import("std");
const Allocator = std.mem.Allocator;
const utils = @import("reflection_utils.zig");

pub fn fromText(comptime Type: type, version: u32, text: []const u8, allocator: ?*Allocator) ?Type {
    var result: Type = undefined;

    var context = FromTextContext{
        .full_text = text,
        .text = text,
        .is_in_container = false,
        .allocator = allocator,
    };

    _ = context.parseHeader(Type, version) orelse return null;
    const end_slice = context.parse(Type, &result);

    if (end_slice == null) {
        return null;
    } else {
        return result;
    }
}

const FromTextContext = struct {
    /// The full input text being parsed.
    full_text: []const u8,

    /// The portion of the input text that has yet to be parsed.
    text: []const u8,

    /// Whether the current value to be parsed is inside a list-like container.
    is_in_container: bool,

    /// Optional allocator to use if any parsed value needs heap memory.
    allocator: ?*Allocator,

    // TODO Could gather errors here?

    fn parseHeader(self: *FromTextContext, comptime Type: type, version: u32) ?[]const u8 {
        self.skipWhitespace();

        if (!self.skipSingleToken(@typeName(Type))) {
            return null;
        }

        self.skipWhitespace();

        // TODO Pretty rough, would be nice to have a string type with small string optimization.
        var buf: [20]u8 = undefined;
        const version_slice = std.fmt.bufPrint(&buf, "v{}", .{version}) catch return null;
        if (!self.skipSingleToken(version_slice)) {
            return null;
        }

        return self.text;
    }

    fn parse(self: *FromTextContext, comptime Type: type, value: *Type) ?[]const u8 {
        switch (@typeInfo(Type)) {
            .Struct => |struct_info| {
                if (comptime utils.isListLike(Type)) {
                    if (self.allocator) |alloc| {
                        // TODO Memory leak if parsing fails - no way for the caller to deinit.
                        // Could have an automated cleanup that would free memory?
                        value.* = Type.init(alloc);
                        std.debug.assert(value.items.len == 0);

                        self.skipWhitespace();
                        if (!self.skipSingleToken("[")) {
                            return null;
                        }

                        while (self.text.len > 0) {
                            self.skipWhitespace();
                            if (std.mem.startsWith(u8, self.text, "]")) {
                                if (!self.skipSingleToken("]")) {
                                    return null;
                                }
                                break;
                            } else if (std.mem.startsWith(u8, self.text, ",")) {
                                if (!self.skipSingleToken(",")) {
                                    return null;
                                }
                            } else {
                                var new_elem = value.addOne() catch return null;
                                var child_context = self.*;
                                child_context.is_in_container = true;
                                self.text = child_context.parse(@TypeOf(new_elem.*), new_elem) orelse return null;
                            }
                        }
                    } else {
                        std.log.err("Need to grow a dynamic list but no allocator provided.", .{});
                        return null;
                    }
                } else {
                    self.skipWhitespace();
                    if (!self.skipSingleToken("{")) {
                        return null;
                    }
                    self.skipWhitespace();

                    inline for (struct_info.fields) |field| {
                        if (!self.skipSingleToken(field.name)) {
                            return null;
                        }
                        if (!self.skipSingleToken(":")) {
                            return null;
                        }
                        self.skipWhitespace();

                        var field_value: *field.field_type = &@field(value.*, field.name);
                        var child_context = self.*;
                        child_context.is_in_container = false;
                        self.text = child_context.parse(field.field_type, field_value) orelse return null;

                        if (!self.skipSingleToken(";")) {
                            return null;
                        }
                        self.skipWhitespace();
                    }

                    self.skipWhitespace();
                    if (!self.skipSingleToken("}")) {
                        return null;
                    }
                }
            },
            .Float => {
                const elem_end_idx = self.findEndOfValue() orelse return null;

                const value_slice = self.text[0..elem_end_idx];
                value.* = std.fmt.parseFloat(Type, value_slice) catch return null;

                self.text = self.text[elem_end_idx..];
            },
            .Int => {
                const elem_end_idx = self.findEndOfValue() orelse return null;

                const value_slice = self.text[0..elem_end_idx];
                value.* = std.fmt.parseInt(Type, value_slice, 10) catch return null;

                self.text = self.text[elem_end_idx..];
            },
            .Bool => {
                const elem_end_idx = self.findEndOfValue() orelse return null;

                const value_slice = self.text[0..elem_end_idx];

                if (std.mem.startsWith(u8, value_slice, "true")) {
                    value.* = true;
                } else if (std.mem.startsWith(u8, value_slice, "false")) {
                    value.* = false;
                } else {
                    std.log.err("Malformed bool value \"{}\".", .{value_slice});
                    return null;
                }

                self.text = self.text[elem_end_idx..];
            },
            .Pointer => |pointer_info| {
                if (pointer_info.size == .Slice) {
                    // TODO Could support this, would have to assume that the user owns the slice memory and allocate it.
                    @compileError("Unsupported slice for writing to text " ++ @typeName(Type));
                } else {
                    @compileError("Unsupported pointer for writing to text " ++ @typeName(Type));
                }
            },
            else => @compileError("Unsupported type for writing to text  " ++ @typeName(Type)),
        }

        return self.text;
    }

    fn skipSingleToken(self: *FromTextContext, token: []const u8) bool {
        if (std.mem.startsWith(u8, self.text, token)) {
            self.text = self.text[token.len..];
            return true;
        }

        const space_or_end_idx = std.math.min(std.mem.indexOf(u8, self.text, " ") orelse std.math.maxInt(usize), self.text.len);

        std.log.err("Expected token \"{}\", found \"{}\", at line {d}.", .{ token, self.text[0..space_or_end_idx], self.getLineNumber() });
        return false;
    }

    fn skipWhitespace(self: *FromTextContext) void {
        self.text = std.mem.trimLeft(u8, self.text, " \t\n");
    }

    fn getLineNumber(self: *const FromTextContext) u32 {
        const slice_ptr = self.text.ptr;
        const preceding_slice = self.full_text[0..(@ptrToInt(slice_ptr) - @ptrToInt(self.full_text.ptr))];

        var line_number: u32 = 1;
        for (preceding_slice) |char| {
            if (char == '\n') {
                line_number += 1;
            }
        }
        return line_number;
    }

    fn closestIndexOf(self: *const FromTextContext, tokens_tuple: anytype) ?usize {
        var lowest_index: usize = std.math.maxInt(usize);

        inline for (tokens_tuple) |token| {
            if (std.mem.indexOf(u8, self.text, token)) |idx| {
                lowest_index = std.math.min(lowest_index, idx);
            }
        }

        if (lowest_index < self.text.len) {
            return lowest_index;
        }

        std.log.err("Missing expected end of value at line {d}.", .{self.getLineNumber()});
        return null;
    }

    fn findEndOfValue(self: *const FromTextContext) ?usize {
        return if (self.is_in_container) self.closestIndexOf(.{ ",", "]" }) else self.closestIndexOf(.{";"});
    }
};
